<?php

namespace App\Http\Controllers;

use App\LuckyDraw;
use App\Prize;
use App\WinningNumber;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class LuckyDrawController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $luckyDraws = LuckyDraw::latest()->get();

        return view('lucky-draw.index', compact('luckyDraws'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $prizes = Prize::all();

        return view('lucky-draw.create', compact('prizes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return void
     */
    public function store()
    {
        request()->validate([
            'prize' => 'required|integer|exists:prizes,id',
            'generate_randomly' => 'required|integer',
            'winning_number' => 'nullable|integer'
        ]);

        $prize = Prize::findOrFail(request('prize'));

        //total number of winners available
        $count = LuckyDraw::where('prize_id', $prize->id)
            ->count();

        //check if prize is available
        if($count === $prize->number_of_winners){
            return back()
                ->with('error', "There is no more winner slot for $prize->name");
        }

        //users who already won lucky draw
        $winners = LuckyDraw::whereDate('created_at', Carbon::today())
            ->get()
            ->pluck('user_id');

        if(request('generate_randomly')){
            if($prize->name === 'Grand Prize'){
                // get the highest winning numbers
                $highest = \DB::table('winning_numbers')
                    ->select('user_id', \DB::raw('count(user_id) as total'))
                    ->whereNotIn('user_id', $winners)
                    ->groupBy('user_id')
                    ->get()->max('total');

                // get users with highest winning numbers
                $highestWinningNumberUsers = \DB::table('winning_numbers')
                    ->select('user_id', \DB::raw('count(user_id) as total'))
                    ->whereNotIn('user_id', $winners)
                    ->groupBy('user_id')
                    ->having('total', $highest)
                    ->get()->pluck('user_id');

                $winningNumber = WinningNumber::whereIn('user_id', $highestWinningNumberUsers)
                    ->inRandomOrder()
                    ->limit(1)
                    ->first();
            }else{
                $winningNumber = WinningNumber::whereNotIn('user_id', $winners)
                    ->inRandomOrder()
                    ->limit(1)
                    ->first();
            }
        }else{
            $winningNumber = WinningNumber::whereNotIn('user_id', $winners)
                ->where('number', request('winning_number'))
                ->first();

            if(empty($winningNumber)){
                return back()
                    ->with('error', "User had won one of the lucky draw.");
            }
        }

        LuckyDraw::create([
            'winning_number_id' => $winningNumber->id,
            'user_id' => $winningNumber->user_id,
            'prize_id' => request('prize')
        ]);

        return redirect()->to(route('lucky-draw.index'))
            ->with('success', 'New Lucky draw has been created.');
    }
}
