<?php

namespace App\Http\Controllers;

use App\WinningNumber;
use Illuminate\Http\Request;

class WinningNumberController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $winningNumbers = WinningNumber::all();

        return view('winning-number.index', compact('winningNumbers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        request()->validate([
            'number' => 'required|digits:4'
        ]);

        $number = request('number');

        if(WinningNumber::whereNumber($number)->exists()){
            return back()->with(
                'error', 'Number existed. Please try another number.'
            );
        }

        auth()->user()
            ->winningNumbers()->create([
                'number' => request('number')
            ]);

        return back()->with(
            'success', 'Added new winning number.'
        );
    }
}
