<?php

namespace App\Http\Controllers;

use App\Prize;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PrizeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $prizes = Prize::all();

        return view('prize.index', compact('prizes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('prize.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return void
     */
    public function store()
    {
        $data = request()->validate([
            'name' => 'required|string',
            'number_of_winners' => 'required|integer'
        ]);

        Prize::create($data);

        return redirect()->to(route('prize.index'))
            ->with('success', 'New Prize has been created.');
    }
}
