<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    public function index()
    {
        $superAdmins = User::role('super admin')->get();

        return view('admin.administrator.index', compact('superAdmins'));
    }

    public function show(User $administrator)
    {
        return view('admin.administrator.show', compact('administrator'));
    }

    public function create()
    {
        $admins = User::role('super admin')->get();

        $users = User::all()->diff($admins);

        return view('admin.administrator.indexUser', compact('users'));
    }

    public function store()
    {
        $user = User::find(request('user_id'))
            ->assignRole('super admin');

        return redirect('admin/administrator')->with('success', 'Assigned admin to '.$user->name);
    }

    public function edit(User $administrator)
    {
        return view('admin.administrator.edit', compact('administrator'));
    }

    public function update(User $administrator)
    {
        request()->validate([
            'name' => 'required|string',
            'own_password' => 'nullable|string',
            'password' => 'required_with:own_password|nullable|string|confirmed',
            'password_confirmation' => 'required_with:password|nullable|string'
        ]);

        $loggedIn_user = auth()->user();

        $name = request('name');
        $password = request('password');

        if(request('own_password')){
            if (!(Hash::check(request('own_password'), $loggedIn_user->password))) {
                return back()->with('error', 'Your user password is incorrect.');
            }

            $administrator->name = $name;
            $administrator->password = $password;
            $administrator->save();
        }

        $administrator->name = $name;
        $administrator->save();

        return redirect('admin/administrator')->with('success', 'Admin detail has been updated.');
    }

    public function destroy(User $administrator)
    {
        $administrator->removeRole('super admin');

        return back()->with('success', 'Removed admin from '.$administrator->name);
    }
}
