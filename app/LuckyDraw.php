<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LuckyDraw extends Model
{
    protected $guarded = [];

    public function prize()
    {
        return $this->belongsTo(Prize::class);
    }

    public function winningNumber()
    {
        return $this->belongsTo(WinningNumber::class);
    }

    public function winner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
