@extends('layouts.admin')

@section('extra-css')
    <link rel="stylesheet" href="{{asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <section class="content-header">
                <div class="col-md-6">
                    <h3>
                        Prize
                    </h3>
                </div>
                <div class="col-md-6" style="padding-top:12px">
                    <a href="{{route('prize.create')}}" class="pull-right btn btn-success btn-sm">
                        New Prize
                    </a>
                </div>
            </section>
            <section class="content">
                <div class="">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-body">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Prize Type</th>
                                        <th>Number of Winners</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($prizes as $index => $prize)
                                        <tr>
                                            <td>{{++$index}}</td>
                                            <td>{{$prize->name}}</td>
                                            <td>{{$prize->number_of_winners}}</td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    @include('layouts._status')
@endsection

@section('extra-script')
    <script src="{{asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script>
        $(function () {
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>
@endsection
