@extends('layouts.admin')

@section('content')
    <div class="content-wrapper">
        <div class="col-md-8 col-md-offset-2">
            <section class="content-header">
                <h1>
                    Create new prize
                </h1>
            </section>

            <section class="content">
                <div class="box box-primary">
                    <form action="{{route('prize.store')}}" method="POST" role="form">
                        @csrf
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name">Prize Types *</label>
                                <input name="name" type="text" class="form-control" id="name" placeholder="Enter prize type">
                            </div>

                            <div class="form-group">
                                <label for="number_of_winners">Number of Winners</label>
                                <input name="number_of_winners" type="number" class="form-control" id="number_of_winners" placeholder="Enter winning number">
                            </div>
                        </div>

                        <div class="box-footer">
                            <a href="{{route('lucky-draw.index')}}" type="button" class="btn btn-primary">Back</a>
                            <button type="submit" class="btn btn-success">Create</button>
                        </div>

                        <div class="box-body">
                            @include('layouts._errors')
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
@endsection
