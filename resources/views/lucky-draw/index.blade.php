@extends('layouts.admin')

@section('extra-css')
    <link rel="stylesheet" href="{{asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="col-md-6">
                    <h3>
                        Lucky Draw
                    </h3>
                </div>
                <div class="col-md-6" style="padding-top:12px">
                    <a href="{{route('lucky-draw.create')}}" class="pull-right btn btn-success btn-sm">
                        New Lucky Draw
                    </a>
                </div>
            </section>
            <section class="content">
                <div class="">
                    <div class="col-xs-12">
                        <div class="box">
                            <!-- /.box-header -->
                            <div class="box-body">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Date</th>
                                        <th>Prize</th>
                                        <th>Winning Number</th>
                                        <th>Winner</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($luckyDraws as $index => $luckyDraw)
                                        <tr>
                                            <td>{{++$index}}</td>
                                            <td>{{$luckyDraw->created_at->format('d M Y')}}</td>
                                            <td>{{$luckyDraw->prize->name}}</td>
                                            <td>{{$luckyDraw->winningNumber->number}}</td>
                                            <td>{{$luckyDraw->winner->name}}</td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    @include('layouts._status')

@endsection

@section('extra-script')
    <script src="{{asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script>
        $(function () {
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>
@endsection
