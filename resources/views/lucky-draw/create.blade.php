@extends('layouts.admin')

@section('content')
    <div class="content-wrapper">
        <div class="col-md-8 col-md-offset-2">
            <section class="content-header">
                <h1>
                    Create new lucky draw
                </h1>
            </section>

            <section class="content">
                <div class="box box-primary">
                    <form action="{{route('lucky-draw.store')}}" method="POST" role="form">
                        @csrf
                        <div class="box-body">
                            <div class="form-group">
                                <label for="prize">Prize Types *</label>
                                <select name="prize" id="prize" class="form-control" required>
                                    <option value="" disabled selected>Please select</option>
                                    @foreach($prizes as $prize)
                                        <option value="{{$prize->id}}">{{$prize->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="generate_randomly">Generate Randomly</label>
                                <select name="generate_randomly" id="generate_randomly" class="form-control" required>
                                    <option value="" disabled selected>Please select</option>
                                    <option value=0>No</option>
                                    <option value=1>Yes</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="winning_number">Winning Number</label>
                                <input name="winning_number" type="number" class="form-control" id="winning_number" placeholder="Enter winning number...">
                            </div>
                        </div>

                        <div class="box-footer">
                            <a href="{{route('lucky-draw.index')}}" type="button" class="btn btn-primary">Back</a>
                            <button type="submit" class="btn btn-success">Create</button>
                        </div>

                        <div class="box-body">
                            @include('layouts._errors')
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
@endsection
