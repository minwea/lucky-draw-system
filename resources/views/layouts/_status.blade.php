@if(session()->has('success'))
    <div class="alert alert-success alert-customize" role="alert" style="position:fixed;right:25px;bottom:25px;">
        <b>{{session('success')}}</b>
    </div>
@endif

@if(session()->has('error'))
    <div class="alert alert-danger alert-customize" role="alert" style="position:fixed;right:25px;bottom:25px;">
        <b>{{session('error')}}</b>
    </div>
@endif
