<footer class="main-footer">
    <div class="pull-right hidden-xs">
    </div>
    <strong>Copyright &copy; 2018 <a href="{{url('admin')}}">{{config('app.name')}}</a>.</strong> All rights
    reserved.
</footer>
