<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            @role('Admin')
            <li>
                <a href="{{route('lucky-draw.index')}}"><i class="fa fa-dashboard"></i> <span>Lucky Draw</span></a>
            </li>
            <li>
                <a href="{{route('prize.index')}}"><i class="fa fa-dashboard"></i> <span>Prize</span></a>
            </li>
            <li>
                <a href="{{route('winning-number.index')}}"><i class="fa fa-dashboard"></i> <span>Winning Number</span></a>
            </li>
            <li>
                <a href="{{url('admin/user')}}"><i class="fa fa-user"></i> <span>User</span></a>
            </li>
            @endrole
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
