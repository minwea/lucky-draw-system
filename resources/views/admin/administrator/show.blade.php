@extends('layouts.admin')

@section('admin', 'active')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="pull-right">
                <a type="button" href="{{url('admin/administrator')}}" class="btn btn-info">Back</a>
            </div>
            <h1>
                Admin
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">Admin Info</h3>
                        </div>

                        <div class="box-body">
                            <dl class="dl-horizontal">
                                <dt>ID</dt>
                                <dd>{{$administrator->id}}</dd>
                                <dt>Name</dt>
                                <dd>{{$administrator->name}}</dd>
                                <dt>Email</dt>
                                <dd>{{$administrator->email}}</dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

@endsection