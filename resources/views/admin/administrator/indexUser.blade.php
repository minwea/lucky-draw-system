@extends('layouts.admin')

@section('extra-css')
    <link rel="stylesheet" href="{{asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('admin', 'active')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="col-md-6">
                    <h3>
                        New Admin
                    </h3>
                </div>
                <div class="col-md-6" style="padding-top:12px">
                    <a href="{{url('admin/administrator')}}" class="pull-right btn btn-info btn-sm">
                        Back
                    </a>
                </div>
            </section>
            <section class="content">
                <div class="">
                    <div class="col-xs-12">
                        <div class="box">
                            <!-- /.box-header -->
                            <div class="box-body">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Name</th>
                                        <th>E-Mail</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($users)
                                        @foreach($users as $index => $user)
                                            <tr>
                                                <td>{{++$index}}</td>
                                                <td>{{$user->name}}</td>
                                                <td>{{$user->email}}</td>
                                                <td>
                                                    <form action="{{url('admin/administrator')}}" method="POST">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="user_id" value="{{$user->id}}">
                                                        <button type="button" class="btn label label-success" data-toggle="modal" data-target="#assign{{$user->id}}">Assign</button>
                                                        <div class="modal modal-info fade" id="assign{{$user->id}}">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span></button>
                                                                        <h4 class="modal-title">Assign Admin</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <p>Assign admin to {{$user->name}} ?</p>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cancel</button>
                                                                        <button type="submit" class="btn btn-outline">Assign</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </td>
                                            </tr>
                                    @endforeach
                                    @endif
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    @include('layouts._status')

@endsection

@section('extra-script')
    <script src="{{asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script>
        $(function () {
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>
@endsection