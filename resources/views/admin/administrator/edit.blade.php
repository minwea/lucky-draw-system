@extends('layouts.admin')

@section('admin', 'active')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit Admin
                <small>Edit</small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"></h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form action="{{url('admin/administrator/'.$administrator->id)}}" method="POST" role="form">
                            @method('patch')
                            @csrf
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="name">Email</label>
                                    <p>{{$administrator->email}}</p>
                                </div>
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input name="name" type="text" class="form-control" id="name" placeholder="Please enter your name" value="{{$administrator->name}}" required>
                                </div>
                                <div class="form-group">
                                    <label for="own_password">Password</label>
                                    <input name="own_password" type="password" class="form-control" id="own_password" placeholder="Please enter your user password if you wish to change password">
                                </div>
                                <div class="form-group">
                                    <label for="password">New Password</label>
                                    <input name="password" type="password" class="form-control" id="password" placeholder="Please enter new password">
                                </div>
                                <div class="form-group">
                                    <label for="password_confirmation">New Password Confirmation</label>
                                    <input name="password_confirmation" type="password" class="form-control" id="password_confirmation" placeholder="Please re-enter new password">
                                </div>
                            </div>

                            <div class="box-footer">
                                <a href="{{url('admin/administrator')}}" type="button" class="btn btn-primary">Back</a>
                                <button type="submit" class="btn btn-success pull-right">Update</button>
                            </div>

                            <div class="box-body">
                                @include('layouts._errors')
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

    @include('layouts._status')

@endsection