@extends('layouts.admin')

@section('extra-css')
    <link rel="stylesheet" href="{{asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('customer', 'active')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="col-md-6">
                    <h3>
                        User Page
                    </h3>
                </div>
            </section>
            <section class="content">
                <div class="">
                    <div class="col-xs-12">
                        <div class="box">
                            <!-- /.box-header -->
                            <div class="box-body">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Name</th>
                                        <th>E-Mail</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($customers)
                                        @foreach($customers as $index => $customer)
                                            <tr>
                                                <td>{{++$index}}</td>
                                                <td>{{$customer->name}}</td>
                                                <td>{{$customer->email}}</td>
                                            </tr>
                                    @endforeach
                                    @endif
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    @include('layouts._status')

@endsection

@section('extra-script')
    <script src="{{asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script>
        $(function () {
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>
@endsection
