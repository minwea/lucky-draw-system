<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Route::post('/winning-number', 'WinningNumberController@store')->name('winning-number.store');

Auth::routes();

Route::view('/admin/login', 'auth.admin.login');
Route::post('/admin/login', 'Auth\AdminLoginController@login')->name('admin.login');
Route::post('/admin/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'role:Admin']], function(){
    Route::name('lucky-draw.')->group(function() {
        Route::get('/', 'LuckyDrawController@index')->name('index');
        Route::get('/lucky-draw/create', 'LuckyDrawController@create')->name('create');
        Route::post('/lucky-draw', 'LuckyDrawController@store')->name('store');
    });

    Route::name('prize.')->group(function() {
        Route::get('/prize', 'PrizeController@index')->name('index');
        Route::get('/prize/create', 'PrizeController@create')->name('create');
        Route::post('/prize', 'PrizeController@store')->name('store');
    });

    Route::get('winning-number', 'WinningNumberController@index')->name('winning-number.index');

    Route::get('user', 'Admin\CustomerController@index');
});
